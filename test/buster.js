var config = module.exports;

config.backend = {
    rootPath: "../",
    environment: "node",
    sources: [],
    tests: [
        "test/backend/*.js"
    ]
};

config.frontend = {
    rootPath: "../",
    environment: "browser",
    sources: [],
    tests: [
        "test/frontend/*.js"
    ]
};