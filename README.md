# JavaScript MUD
##### Copyright 2013 Ryan Zander. All rights reserved. See LICENSE for more information.

This is an attempt at merging the gameplay of MajorMUD with modern web technologies based around JavaScript. I will expand on this as time goes on.

---

### How to setup:

    $ git clone https://bitbucket.org/Exidious/javascript-mud.git .
    $ npm install
    $ jake.bat

### How to use:

    Nothing yet...