// Copyright (c) 2013 Ryan Zander. All rights reserved. See LICENSE for more information.

var shell = require("shelljs");

// Constants
var TOOLS_PATH = "/node_modules/.bin";
var SUPPORTED_BROWSERS = ["IE 10", "Firefox 23", "Chrome 31"];

// Global setup
shell.env.PATH += shell.pwd() + TOOLS_PATH;

// Start of build steps
/* ------------------------------------------------------------------------- */

task("default", ["lint", "test"], function() {
	console.log("\n\n" + colorize("green", "--- BUILD SUCCESSFUL! ---"));
});

desc("Lint everything");
task("lint", [], function() {
	printHeader("Linting...");

	var files = new jake.FileList();
	files.include("*.js");
	files.include("src/**/*.js");
	files.include("test/**/*.js");
	files.toArray().forEach(function(file) {
		execute("jshint " + file, "Build failed while linting!");
	});

	passed();
});

desc("Test everything");
task("test", ["test-server", "test-client"]);

desc("Test server components");
task("test-server", [], function() {
	printHeader("Testing server...");

	execute("buster-test -g backend -v", "Build failed while testing the server!");

	passed();
});

desc("Test client components");
task("test-client", [], function() {
	printHeader("Testing client...");

	var command = "buster-test -g frontend -v";
	var failureMessage = "Build failed while testing the client!";
	var commandOutput;

	execute(command, failureMessage, function(output) { commandOutput = output; });
	passed();

	var t = jake.Task["test-client-runtimes"];
	t.invoke.call(t, commandOutput);
});

//desc("Verify supported browsers are tested");
task("test-client-runtimes", [], function(clientTestOutput) {
	printHeader("Verify supported browsers were tested...");

	SUPPORTED_BROWSERS.forEach(function(browser) {
		if (clientTestOutput.indexOf(browser) == -1) {
			failed(browser + " NOT found!");
		} else {
			console.log("-> " + browser + " found!");
		}
	});

	passed();
});

// End of build steps
/* ------------------------------------------------------------------------- */

function execute(command, failureMessage, callback) {
	var process = shell.exec(command);

	if (callback) {
		callback(process.output);
	}

	if (process.code !== 0) {
		failed(failureMessage);
	}
}

function passed(string) {
	if (!string) string = "PASSED!";
	console.log(colorize("green", string));
}

function failed(string) {
	if (!string) string = "FAILED!";
	fail(colorize("red", string));
}

function printHeader(string) {
	console.log("\n" + colorize("white", string));
}

function colorize(color, string) {
	var knownColors = {
		"red": "\033[91m",
		"green": "\033[92m",
		"white": "\033[97m"
	};

	if (knownColors[color]) {
		return knownColors[color] + string + "\033[39m";
	} else {
		throw new Error("Don't know the ANSI escape code for the color '" + color + "'");
	}
}